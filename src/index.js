import express from 'express'
import fetch from 'isomorphic-fetch'
import Promise from 'bluebird'
import _ from 'lodash'

import { canonize } from './canonize'

const __DEV__ = false;

const app = express();

app.get('/canonize', (req, res) => {
	const result = canonize(req.query.url)
	res.json(result);
});

const baseUrl = 'http://pokeapi.co/api/v2';
const pokemonFields = ['id', 'name', 'base_experience', 'height', 'is_default', 'order', 'weight'];

async function getPokemons(url, i = 0) {
	const response = await fetch(url);
	const page = await response.json();
	const pokemons = page.results;
	if (__DEV__ && i > 3) {
		return pokemons;
	}
	console.log(page.next);
	if (page.next) {
		const pokemonsNext = await getPokemons(page.next, i++);
		return [
			...pokemons,
			...pokemonsNext
		];
	}
	return pokemons;
}

async function getPokemonInfo(url) {
	const response = await fetch(url);
	const pokemon = await response.json();
	return pokemon;
}

app.get('/', async (req, res) => {
	try {
		const pokemonsUrl = `${baseUrl}/pokemon`;
		const pokemonsInfo = await getPokemons(pokemonsUrl);

		const pokemonsPromises = pokemonsInfo.map((info) => {
			return getPokemonInfo(info.url);
		});

		const pokemonsFull = await Promise.all(pokemonsPromises);
		const pokemons = pokemonsFull.map((poke) => {
			return _.pick(poke, pokemonFields);
		});
		const sortPokemons = _.sortBy(pokemons, poke => poke.weight);

		res.json(sortPokemons);
	} catch (err) {
		console.log(err);
		return res.json({ err });
	}
});

app.listen(3000, function () {
	console.log('App listening on port 3000...');
});
/*
const array = [
	'https://telegram.me/seclace',
	'https://vk.com/cheburasha',
	'https://bitbucket.org/seclace',
	'seclace',
];
array.forEach((url) => {
	console.log(canonize(url));
});*/